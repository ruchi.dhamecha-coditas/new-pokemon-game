import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import style from  './App.module.scss'
import Header from './component/header/Header'
import Main from './component/main/Main'
const App = ()=>{
  return(
    <>
      <Header/>
      <Main/>
    </>
  )
}

export default App

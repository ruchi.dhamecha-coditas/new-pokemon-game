import style from './header.module.scss'

const App = ()=>{
    return(
      <>
        <header className={style.Header}>
          <h2>Pokemons</h2>
        </header>
      </>
    )
  }
  
  export default App
  
import style from './item.module.scss'
import { itemProps } from './item.types'
const Item=({id,title,onclick}:itemProps)=>{
    return(

        <>
        <div className={style.Item} onClick={()=>onclick(id)}>
           <p>{id}</p>
           <p>{title}</p>
        </div>
           
            
        </>
    )
}
export default Item
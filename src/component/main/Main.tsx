import style from './main.module.scss'
import List from '../List/List'
import axios from "axios"
import { useEffect, useState } from 'react'

export interface pokemonData{
    name:string
    id:number
    caught?:boolean
}

const Main=()=>{
    
    const[pokemonData,setPokemondata] = useState<pokemonData[]>([]);

    const getdata = async ():Promise<void>=>{
        try{
            const {data} = await axios.get<pokemonData>(
                'url',
                {
                    headers: {
                        "ngrok-skip-browser-warning": "skip-browser-warning",
                      },
                }
            );
            setPokemondata([
                {
                    "id": 1,
                    "name": "Bulbasaur"
                  },
                  {
                    "id": 2,
                    "name": "Ivysaur"
                  },
                  {
                    "id": 3,
                    "name": "Venusaur"
                  },
                  {
                    "id": 4,
                    "name": "Charmander"
                  },
                  {
                    "id": 5,
                    "name": "Charmeleon"
                  },
                  {
                    "id": 6,
                    "name": "Charizard"
                  },
                  {
                    "id": 7,
                    "name": "Squirtle"
                  },
                  {
                    "id": 8,
                    "name": "Wartortle"
                  },
                  {
                    "id": 9,
                    "name": "Blastoise"
                  }
            ]);
        }
        catch(error:any){
            console.log(error.message)
        }
    }
    useEffect(()=>{
        getdata();
    },[])


    function onclickbutton(id:number){
        console.log("nkndk")
        setPokemondata(
            pokemonData.map((pokemon)=>
                pokemon.id===id? {...pokemon, caught:!pokemon.caught}:pokemon
            )
        )
    }

    const allPokemon=pokemonData.filter((pokemon)=>!pokemon.caught);
    const caughtPokemon=pokemonData.filter((pokemon)=>pokemon.caught);


    return(

        <>
        <div className={style.Main}>
            <List title="All pokemon" data={allPokemon} onclickbutton={onclickbutton}/>
            <List title="Caught pokemon" data={caughtPokemon} onclickbutton={onclickbutton}/>
        </div>
           
            
        </>
    )
}
export default Main
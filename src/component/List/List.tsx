import style from './list.module.scss'
import Item from '../item/Item'
import { ListProps } from './list.type'
const List = (props: ListProps) => {
    return (

        <>
            <div className={style.List}>
            <h3>{props.title}</h3>
            {
                props.data.map((l) => {
                    return (
                        <>
                            <Item id={l.id} title={l.name} onclick={()=>
                                props.onclickbutton(l.id)}/>
                        </>
                    )
                })
            }

            </div >
           
    
        </>
    )
}
export default List
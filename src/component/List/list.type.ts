
import {pokemonData} from '../main/Main'
export interface ListProps{
    title:string
    data:pokemonData[]
    onclickbutton:(id:number)=>void
}